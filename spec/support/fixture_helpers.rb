# frozen_string_literal: true

module FixtureHelpers
  FILE_FIXTURE_PATH = './spec/fixtures/files'

  def file_fixture(fixture_name)
    path = Pathname.new(File.join(FILE_FIXTURE_PATH, fixture_name))

    if path.exist?
      path
    else
      msg = "the directory '%s' does not contain a file named '%s'"
      raise ArgumentError, format(msg, [FILE_FIXTURE_PATH, fixture_name])
    end
  end
end
