# frozen_string_literal: true

require 'rspec'

describe XmlParser do
  let(:filename) { './callouts/2021-09-16/2021-09-16T05:54:31-123.xml' }
  let(:callback_path) { '/zuora_callback/order_processed' }

  let(:hash) do
    { create_time: '2021-09-16T05:54:31',
      event_context: {
        "<account.number>": '123', "<sold_to_contact.work_email>": 'test@example.com',
        "<send_xml>": xml
      },
      request_url: "http://localhost:5000#{callback_path}",
      event_category: 'test', attempted_num: 1 }
  end

  let(:xml) { file_fixture('sample_callout.xml').read }
  let(:zuora_user_id_config) { '6b9dd7dc016ba9' }

  before do
    allow(ENV).to receive(:[]).with("ZUORA_USER_ID").and_return(zuora_user_id_config)
  end

  after do
    File.delete(filename) if File.exist?(filename)
  end

  it 'parses Zuora callout hashes' do
    allow(FileUtils).to receive(:mkdir_p)
    expect(File).to receive(:write).with(filename, xml)

    described_class.parse_and_save(hash) do |response, path|
      expect(response).to eq(xml)
      expect(path).to eq(callback_path)
    end
  end

  shared_examples 'does not return callout hashes' do
    it do
      described_class.parse_and_save(hash) do |response, path|
        expect(response).to be_nil
        expect(path).to be_nil
      end
    end
  end

  context 'when callout response is not the current user' do
    let(:zuora_user_id_config) { 'another-user' }

    it_behaves_like 'does not return callout hashes'
  end

  context 'when callout response is already processed' do
    before do
      hash[:attempted_num] = 2
    end

    it_behaves_like 'does not return callout hashes'
  end

  describe 'validations' do
    context 'when work email is not present' do
      let(:hash) do
        { create_time: '2021-09-16T05:54:31',
          event_context: {
            "<account.number>": '123',
            "<send_xml>": xml
          },
          request_url: "http://localhost:5000#{callback_path}",
          event_category: 'test', attempted_num: 1 }
      end

      it 'writes callout response to file' do
        allow(FileUtils).to receive(:mkdir_p)
        expect(File).to receive(:write)

        described_class.parse_and_save(hash) do |response, path|
          expect(response).to eq(xml)
          expect(path).to eq(callback_path)
        end
      end
    end
  end
end
