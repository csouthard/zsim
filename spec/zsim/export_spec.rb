# frozen_string_literal: true

require 'rspec'

describe Export do
  it 'send XML to CustomersDot' do
    path = '/zuora_callback/subscription_update'
    expect(HTTParty).to receive(:post).with("http://localhost:5000#{path}",
                                            { basic_auth: { password: nil, username: nil },
                                              body: '<xml></xml>',
                                              headers: { 'Content-type' => 'text/xml' },
                                              timeout: 10 })

    described_class.send_callout(xml: '<xml></xml>', callout_path: path)
  end
end
