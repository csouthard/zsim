ARG RUBY_VERSION
FROM ruby:$RUBY_VERSION-alpine AS builder

# Nokogiri's build dependencies
RUN apk add --update \
    build-base \
    libxml2-dev \
    libxslt-dev

COPY Gemfile Gemfile.lock ./

RUN bundle config set without "development test" && \
    bundle install --jobs=3 --retry=3

FROM ruby:$RUBY_VERSION-alpine

RUN apk add --update tzdata

RUN adduser -D zsim
USER zsim
WORKDIR /home/zsim
COPY --from=builder /usr/local/bundle/ /usr/local/bundle/
COPY --chown=zsim . ./

CMD ["./bin/zsim"]
