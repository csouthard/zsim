# frozen_string_literal: true

require 'date'
require 'nokogiri/xml/builder'
require 'iron_bank'

module IronBank
  module Actions
    # Use the subscribe call to bundle information required to create at least
    # one new subscription
    # https://www.zuora.com/developer/api-reference/#operation/Action_POSTsubscribe
    # (DateTime.now - 3).strftime('%FT%T')
    class NotificationCallout < Action
      public_class_method :new

      def initialize(next_time:)
        @next_time = next_time
        @start_time = next_time.take

        super
      end

      def call
        @end_time = ZuoraTime.now.strftime('%FT%T')
        @body = IronBank.client.connection.get(endpoint, params).body

        raise ::IronBank::UnprocessableEntityError, errors unless success?

        callouts = IronBank::Object.new(body).deep_underscore[:callout_histories]

        @next_time.put(@end_time)

        callouts
      end

      private

      def params
        {
          'startTime': @start_time,
          'endTime': @end_time
        }
      end

      def success?
        @body[:success] || @body['success']
      end

      def errors
        { errors: @body['reasons']&.first&.fetch('message') || @body[:errors] }
      end

      def endpoint
        'v1/notification-history/callout'
      end
    end
  end
end
