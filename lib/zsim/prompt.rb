# frozen_string_literal: true

require 'pastel'
require 'tty-font'
require 'tty-prompt'

module Zsim
  class Prompt
    INITIAL_OPTIONS = [
      'Listen/Replay new callouts',
      { name: 'Replay last', disabled: '(Not Implemented)' },
      'Show last',
      { name: 'Configuration', disabled: '(Not Implemented)' },
      'Exit'
    ].freeze

    attr_reader :options, :prompt

    def initialize
      refresh

      @options = INITIAL_OPTIONS.dup
      @prompt = TTY::Prompt.new
    end

    def execute(show_prompt: true)
      if show_prompt
        run_choices
      else
        run_default_choice
      end
    end

    private

    def refresh
      system('clear')

      font = TTY::Font.new(:starwars)
      pastel = Pastel.new

      puts pastel.yellow(font.write('ZSim'))
      puts pastel.bold(" Version #{Zsim::VERSION} - Experimental\n")
    end

    def run_choices
      choice = prompt.select('', options, cycle: true, filter: true, echo: false)

      refresh

      process_choice(choice)

      run_choices
    end

    def callout
      @callout ||= Callout.new
    end

    def process_choice(choice)
      case choice
      when 'Listen/Replay new callouts'
        choice_listen
      when 'Stop listening/Replaying callouts'
        choice_stop_listen
      when 'Show last'
        choice_show_last
      when 'Exit'
        exit(0)
      end
    end

    def choice_listen
      callout.listen_to_new

      options.delete('Listen/Replay new callouts')
      options.prepend('Stop listening/Replaying callouts')

      prompt.warn('Listening')
    end

    def choice_stop_listen
      callout.stop

      options.delete('Stop listening/Replaying callouts')
      options.prepend('Listen/Replay new callouts')

      prompt.warn('Stopped')
    end

    def choice_show_last
      system('clear')

      last_file = Dir.glob('./callouts/**/*').max_by { |f| File.ctime(f) }
      if last_file
        system('xmllint', last_file)
      else
        prompt.warn('No previous callout files found')
      end

      prompt.keypress('Press any key to continue')

      refresh
    end

    def run_default_choice
      callout.listen_to_new

      prompt.warn('Listening')
      prompt.keypress('Press any key to stop')
    end
  end
end
