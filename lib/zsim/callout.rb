# frozen_string_literal: true

class Callout
  def initialize
    @next_start = Concurrent::MVar.new(ZuoraTime.now.strftime('%FT%T'))
  end

  def listen_to_new
    @task = Concurrent::TimerTask.new(execution_interval: Config.refresh * 60, timeout_interval: 60) do
      notifications = IronBank::Actions::NotificationCallout.new(next_time: @next_start)
      XmlParser.parse_and_save(notifications.call) { |xml, path| Export.send_callout(xml: xml, callout_path: path) }
    end

    @task.add_observer(CalloutObserver.new)
    @task.execute
  end

  def stop
    return unless @task

    @task.shutdown
    sleep 1 while @task&.running?
    @task = nil
  end
end
