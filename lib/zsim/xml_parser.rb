# frozen_string_literal: true

require 'nokogiri/xml/builder'
require 'fileutils'
require 'date'

module XmlParser
  def self.parse_and_save(hashes, &block)
    [hashes].flatten.compact.each do |hash|
      next if Config.ignore_errored_callouts && hash[:response_code].to_s.start_with?('5')
      next unless hash[:request_url] && Regexp.new(Config.request_url_regex).match?(hash[:request_url])
      next unless hash[:event_category] && Regexp.new(Config.event_category_regex).match?(hash[:event_category])
      next unless hash[:attempted_num] == 1

      next if hash[:event_context][:'<sold_to_contact.work_email>'] &&
        !Regexp.new(Config.sold_to_contact_email_regex).match?((hash[:event_context][:'<sold_to_contact.work_email>']))

      save_xml(hash, &block)
    end
  end

  def self.parse_xml(hash)
    hash[:event_context].each do |object|
      param = Nokogiri::XML(object[0].to_s).children.first.name
      next if param != "send_xml"

      return object[1]
    end; nil
  end

  def self.filename(hash)
    dir = "./callouts/#{Date.parse(hash[:create_time])}"
    context = hash[:event_context][:"<account.number>"] || hash[:event_context][:"<process.id>"]
    name = "#{hash[:create_time]}-#{context}"
    FileUtils.mkdir_p(dir)

    "#{dir}/#{name}.xml"
  end

  def self.save_xml(hash)
    file = filename(hash)
    xml = parse_xml(hash)
    return unless current_user?(xml)

    File.write(file, xml) unless File.exist?(file)
    yield(xml, callout_path(hash))
  end

  def self.callout_path(hash)
    URI.parse(hash[:request_url])&.path
  end

  def self.current_user?(xml)
    doc = Nokogiri::XML(xml)
    user_id = doc.xpath('//parameter[@name="creator_id"]').text

    user_id == ENV['ZUORA_USER_ID']
  end

  private_class_method :parse_xml
  private_class_method :filename
  private_class_method :save_xml
end
