# frozen_string_literal: true

require 'httparty'

module Export
  def self.send_callout(xml:, callout_path:)
    HTTParty.post(
      "#{Config.customers_dot_callout_url}#{callout_path}",
      body: xml,
      headers: { 'Content-type' => 'text/xml' },
      basic_auth: { username: ENV['CUSTOMERS_DOT_USER'], password: ENV['CUSTOMERS_DOT_PASSWORD'] },
      timeout: Config.customers_dot_timeout
    )
  end
end
