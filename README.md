# ZSim

ZSim (Zuora Simulator) is an application that forwards [Zuora Callouts](https://knowledgecenter.zuora.com/Central_Platform/Notifications/C_Configure_Callout_Notifications) from Zuora into your local (or other) instance without having any listening port.

This means you don't need to use any tunneling or open any port on your computer to receive callouts.

## How it works

```mermaid
sequenceDiagram
  Zuora-->>+CustomersDot: Failed callout
  ZSim->>+Zuora: Poll for callouts
  Zuora->>+ZSim: Receive JSON callouts from the API
  ZSim->>+CustomersDot: Filtered XML callouts
```

## Pre-installation instructions

- Ensure `ngrok` is not running simultaneously. As ZSim polls only failed callouts, your `ngrok` instance should be down in order for ZSim to receive it.
- If you are installing on an M1 MacBook and using rbenv to manage ruby versions, make sure you do the setup from a terminal opened using Rosetta

## Installation

```bash
git clone git@gitlab.com:jameslopez/zsim.git
cd zsim
bundle install
cp .env.example .env # edit this configuration file
```

Environment configuration values:

1. Use the `ZUORA_CLIENT_ID` and `ZUORA_CLIENT_SECRET` found in the `development` section of `secrets.yml` in the [CustomersDot](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/0ece6b000c029a57d9c60e1ca65d8fc952d49dbf/config/secrets.yml.example#L80) project.
1. Set `ZUORA_USER_ID` to the ID associated with your Zuora user. This can be obtained by accessing the [Manage Users](https://apisandbox.zuora.com/apps/UserLogin.do?method=list) dashboard in Zuora sandbox. Select your user name, and copy the `id` from the URL.
   For e.g. if the url associated with your user is `https://apisandbox.zuora.com/apps/UserLogin.do?method=view&id=1234`, `ZUORA_USER_ID` should be set to `1234`
   If you do not have Administrator access to Zuora sandbox, please message in [#s_fulfillment](https://gitlab.slack.com/archives/CMJ8JR0RH) slack channel.
1. Use any one of the local CustomersDot login credentials for `CUSTOMERS_DOT_USER` and  `CUSTOMERS_DOT_PASSWORD`.

**Note:** In case you do not have values for the Zuora related variables, please message in [#s_fulfillment](https://gitlab.slack.com/archives/CMJ8JR0RH) slack channel.

**Migrating from ngrok:**

1. If using GDK
   1. Update the environment variable `CUSTOMER_PORTAL_URL` from your ngrok url to `http://localhost:5000` or any other port of your choice.
   1. Restart GDK.
   1. Update the Callback URL in your OAuth application.
      1. This could be set at the Instance level (`http://localhost:3000/admin/applications`) or as user preferenced as [described here](https://gitlab.com/gitlab-org/customers-gitlab-com/blob/b229a5dce2858fbbd2a82290044213c2a0a89370/doc/setup/installation_steps.md#step-34-oauth-authorized-application).
      1. Edit the application's Redirect URI using your local CDot address e.g. `http://localhost:5000/auth/gitlab/callback`.
1. Update your payment page URL in Zuora Sandbox:
   1. Visit [Hosted Pages configuration](https://apisandbox.zuora.com/apps/HostedPageLite.do) on Zuora Sandbox
   1. Find your hosted page from the list and select `Edit`
   1. Update "Hosted Domain" to your local CDot address e.g. `http://localhost:5000`

## Running

After setting the environment variables, edit `config.yml` if you want to modify the default options.

```bash
bin/zsim
```

Select `Listen/Replay new callouts`

### Listen to callouts by default

To start listening to callouts by default (without prompt), set the environment variable `PROMPT` to `false`.

## Contributing
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
